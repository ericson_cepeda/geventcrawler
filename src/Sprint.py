# -*- coding: iso-8859-15 -*-
from re import match


__authors__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

import re
from base.airbase import AirBase
import ext_libs
from base.exceptons import AirIOException

class EasyFlyApiBadResponse(AirIOException):
    """
        when this page (http://www.easyfly.com.co/desktop/wsDiscriminacion) have bad response, raise a error

    """
    pass

class Sprint(AirBase):

    name = "Sprint"
    subDomain = ""
    domain = "<subDomain>.sprint3.com"
    host = "<subDomain>.sprint3.com"
    origin = "http://<subDomain>.sprint3.com/"
    refer = "http://<subDomain>.sprint3.com/package/content/kernel_anon_logon.asp"
    main_url = "http://<subDomain>.sprint3.com/"
    search_url = 'http://<subDomain>.sprint3.com/package/content/kernel_sales_catalogue_indexBrowse.asp'
    category_url = 'http://<subDomain>.sprint3.com/package/content/kernel_sales_catalogue_indexBrowse_preview.asp?intKernel_catalogueHeading_PK='
    product_link = 'http://<subDomain>.sprint3.com/package/content/'
    #'AUCK','WELL','SPEC'
    accepted_headings = ['PARTNR.','DESCRIPTION','HLDS.','PL201','SPEC','W/S','RRP']
    accepted_image_headings = ['HLDS.']
    
    #headingsCompiled = False

    def get_data(self, **kwargs):
        """
            see AirBase.get_data_wrapper definition
            return
                {dict} the flights of Sprint for a particular day, route an adult numbers
s        """
        self.subDomain = kwargs.get("subDomain", 'empty')
        self.refer = str(self.refer).replace('<subDomain>', self.subDomain)
        self.main_url = str(self.main_url).replace('<subDomain>', self.subDomain)
        self.search_url = str(self.search_url).replace('<subDomain>', self.subDomain) 
        self.category_url = str(self.category_url).replace('<subDomain>', self.subDomain) 
        self.product_link = str(self.product_link).replace('<subDomain>', self.subDomain) 
        
        self.domain = str(self.domain).replace('<subDomain>', self.subDomain)
        self.host = str(self.host).replace('<subDomain>', self.subDomain)
        self.origin = str(self.origin).replace('<subDomain>', self.subDomain)
        # fill form for query the available flights for a day, route and adult numbers
        data = {}     
        
        self.headingsCompiled = False
        
#        filename = self.subDomain+'.csv'
#        if os.path.isfile(filename):
#            os.remove(filename)
#        self.file = open(filename,'w')
#        self.file.write(','.join(str(x) for x in AirBase.attributes_base.keys())+'\n')

        self.req(self.main_url, data=data, method="GET", callback=self.process_html)

    def process_html(self, response):

        if response.status_code == 200:

            data = {
                "x"                      : 0,
                "y"                      : 0,
                "strRedirectURL"           : '',
                "strNextAction"            : 'processRequest',
                "strPassword"              : AirBase.sites[self.main_url]['pass'],
                "strUsername"              : AirBase.sites[self.main_url]['user']
            }
            
            self.req(self.refer, data=data, method="POST", callback=self.after_login)
        else:
            # TODO: process bad response
            pass

    def after_login(self, response):
        if response.status_code == 200:
            self.req(self.search_url, data={}, method="POST", callback=self.extract_info)
    
    def extract_info(self, response):     
        if response.status_code == 200:
            doc = ext_libs.BeautifulSoup(response.content)
            regex_categories = re.compile(r"catalogue\.add\([0-9]+,[0-9]+,'([\w\s]+)','([0-9]+)'\);",re.MULTILINE)
            categories = doc.find("script",text=regex_categories)
            #print categories
            #self.file.write('|'.join(str(x) for x in AirBase.attributes_base.keys())+'\n')
            match = regex_categories.findall(str(categories))
            for category in match:
                self.req(self.category_url+category[1], data={}, method="GET", callback=self.process_products_link)

    def process_products_link(self, response):
        doc = ext_libs.BeautifulSoup(response.content)
        products = doc.findAll("a",attrs={"target":"_top"})
        for product in products:
            self.req(self.product_link+product['href'], data={}, method="GET", callback=self.process_product_page)

    def process_product_page(self, response):
        doc = ext_libs.BeautifulSoup(response.content, convertEntities=ext_libs.BeautifulSoup.HTML_ENTITIES)
        stockListForm = doc.find("form",attrs={"name":"list"})
        if stockListForm:
            stockListTable = stockListForm.find("table",attrs={"class":"full_width"})
            if stockListTable:
                
                #regex_heading = re.compile(r'()',re.MULTILINE)list_nav
                headings = stockListTable.findAll("td",attrs={"class":"list_heading"})
                headingsQuantity = len(headings)
                #print headingsQuantity
                records = stockListTable.findAll("tr",attrs={"valign":"top"})
                
                currentPage = stockListTable.find("span",attrs={"class":"list_nav_act"})
                if currentPage:
                    pageNumber = int(currentPage.getText())
                    nextPage = pageNumber+1
                    regex_navigation = re.compile("intScrollToPage=("+str(nextPage)+"){1}$")
                    navigationPages = stockListTable.findAll("td",attrs={"class":"list_nav"})
                    #for navigationPage in navigationPages:
                    if len(navigationPages) > 1:
                        navigationLink = navigationPages[1].find("a",attrs={"href":regex_navigation})
                        if navigationLink:
                            self.req(self.product_link+navigationLink['href'], data={}, method="GET", callback=self.process_product_page)
                
                #headingsString = ""
                for record in records:
                    recordTuple = list(AirBase.recordTupleBase)
                    recordAttributes = record.findAll('td')
                    if len(recordAttributes) == headingsQuantity:
                        for index, heading in enumerate(headings):
                            headingText = heading.getText().upper()
                            if headingText in self.accepted_headings:
#                                if self.headingsCompiled == False:
#                                    headingsString+=headingText+"|"
                                regex_quantity = re.compile(r"^>?([0-9]{1,2})$")
                                recordText = recordAttributes[index].getText()
                                match = regex_quantity.search(recordText)
                                headingImage = recordAttributes[index].find('img')
                                if headingImage:
                                    recordTuple[AirBase.attributes_base[headingText]] = headingImage['title'] if 'title' in dict(headingImage.attrs) else "Not Available"
                                else:
                                    recordTuple[AirBase.attributes_base[headingText]] = recordText
                                
                                #recordTuple += "|"
#                        if self.headingsCompiled == False:
#                            self.file.write(headingsString+'\n')
#                        self.headingsCompiled = True
                        #recordTuple = recordTuple[:-1]
                        self.process_tuple(recordTuple)

    def format_date(self, utcTime):

        pass


if __name__ == "__main__":
    """ use for do quick test and check your code without involve the internal app execution routines """

    import logging
    logging.basicConfig(level=logging.NOTSET)

    # tell to app that avoid uses the internal execution routines
    Sprint.developer_mode = True
    # do query
    #The sites with specials are dold, darbi, northern and nationwide
    subDomains = ["darbi","eurobike","nationwide",'northern','whites','eurotred']
    print "Script started."
    for subDomain in subDomains:
        Sprint.get_data(subDomain = subDomain)

    # wait till all the works are done
    Sprint.global_pool.join()
    print "Script finished."
    
    Sprint.send_files(subDomains)
