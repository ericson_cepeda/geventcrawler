__author__ = 'Ericson Cepeda <ericson.cepeda@gmail.com>'
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright (c) 2012, Viryan S.A.'

import  ext_libs

def remove_tags(soup):
    VALID_TAGS = ''
    for tag in soup.findAll(True):
        if tag.name not in VALID_TAGS:
            for i, x in enumerate(tag.parent.contents):
                if x == tag: break
            else:
                print "Can't find", tag, "in", tag.parent
                continue
            for r in reversed(tag.contents):
                tag.parent.insert(i, r)
            tag.extract()
    return soup.renderContents()