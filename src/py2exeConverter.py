# setup.py
from distutils.core import setup
import py2exe

options = { "py2exe": {
                "includes": ['greenlet','BeautifulSoup'],
				'compressed':1,  
				'bundle_files': 1, 
				'dist_dir': "exe/dist/dir"
				'dll_excludes'
          }}

setup(console=["mainScript.py"], options=options,zipfile = None)