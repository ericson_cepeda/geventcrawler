# -*- coding: iso-8859-15 -*-
from base.airbase import AirBase

__authors__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

import os
from Sprint import Sprint
from Unlimited import Unlimited
from VmNet import VmNet
from Dold import Dold

class MainScript():

    @staticmethod
    def run_scripts():
        import logging
        logging.basicConfig(level=logging.NOTSET)
    
        filename = AirBase.fileName+'.csv'
        if os.path.isfile(filename):
            os.remove(filename)
        generalFile = open(filename,'w')
        generalFile.write(','.join(str(x) for x in AirBase.generalHeaders)+'\n')
            
        #self.file.write('|'.join(str(x) for x in AirBase.attributes_base.keys())+'\n')
    
        # tell to app that avoid uses the internal execution routines
        Sprint.developer_mode = True
        # do query
        #The sites with specials are dold, darbi, northern and nationwide
        subDomains = ["darbi","eurobike","nationwide",'northern','whites','eurotred']
        print "Sprint script started."
        for subDomain in subDomains:
            Sprint.get_data(subDomain = subDomain, compilationFile = generalFile)
    
        # wait till all the works are done
        #Sprint.global_pool.join()
        #print "Sprint script finished."
        
        
        # tell to app that avoid uses the internal execution routines
        VmNet.developer_mode = True
        # do query
        subDomains = ["crownkiwi","offroadimports","sevenee"]
        
        print "VmNet script started."
        for subDomain in subDomains:
            VmNet.get_data(subDomain = subDomain, compilationFile = generalFile)
    
        # wait till all the works are done
        #VmNet.global_pool.join()
        #print "VmNet script finished."
        
        
        # tell to app that avoid uses the internal execution routines
        Unlimited.developer_mode = True
        # do query
        print "Unlimited script started."
        Unlimited.get_data(compilationFile = generalFile)
    
        # wait till all the works are done
        #Unlimited.global_pool.join()
        #print "Unlimited script finished."
        
        # tell to app that avoid uses the internal execution routines
        Dold.developer_mode = True
        # do query
        print "Dold script started."
        Dold.get_data(compilationFile = generalFile)
        
        AirBase.global_pool.join()
        AirBase.send_files([AirBase.fileName])


if __name__ == "__main__":
    """ use for do quick test and check your code without involve the internal app execution routines """

    import logging
    logging.basicConfig(level=logging.NOTSET)

    # tell to app that avoid uses the internal execution routines
    MainScript.run_scripts()
