# -*- coding: iso-8859-15 -*-


__authors__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

import re
from base.airbase import AirBase
import ext_libs
from base.exceptons import AirIOException

class EasyFlyApiBadResponse(AirIOException):
    """
        when this page (http://www.easyfly.com.co/desktop/wsDiscriminacion) have bad response, raise a error

    """
    pass

class VmNet(AirBase):

    name = "VmNet"
    domain = "vm.net.nz"
    host = "vm.net.nz"
    origin = "http://vm.net.nz/"
    refer = "http://vm.net.nz/<subDomain>/welcome"
    main_url = "http://vm.net.nz/<subDomain>/welcome"
    search_url = 'http://vm.net.nz/<subDomain>/vm?task=doItems&function=searchItems'
    product_url = 'http://vm.net.nz/<subDomain>/vm?task='
    
    homePage_url = 'http://vm.net.nz/<subDomain>/main.htm'
    sidebar_url = 'http://vm.net.nz/<subDomain>/vm?task=doStartup&function=displayCustomerSidebar'
    intro_url = 'http://vm.net.nz/<subDomain>/vm?task=doStartup&function=displayCustomerIntro'
    
    subDomain = None
    headingsCompiled = False

    accepted_classes = ['tddata']

    def get_data(self, **kwargs):
        """
            see AirBase.get_data_wrapper definition
            return
                {dict} the flights of VmNet for a particular day, route an adult numbers
s        """
        self.subDomain = kwargs.get("subDomain", '')

        self.domain = str(self.domain).replace('<subDomain>', self.subDomain)
        self.host = str(self.host).replace('<subDomain>', self.subDomain)
        self.origin = str(self.origin).replace('<subDomain>', self.subDomain)
        self.refer = str(self.refer).replace('<subDomain>', self.subDomain)
        self.main_url = str(self.main_url).replace('<subDomain>', self.subDomain)
        self.search_url = str(self.search_url).replace('<subDomain>', self.subDomain) 
        self.product_url = str(self.product_url).replace('<subDomain>', self.subDomain) 
        
        self.homePage_url = str(self.homePage_url).replace('<subDomain>', self.subDomain)
        self.sidebar_url = str(self.sidebar_url).replace('<subDomain>', self.subDomain) 
        self.intro_url = str(self.intro_url).replace('<subDomain>', self.subDomain)  
        # fill form for query the available flights for a day, route and adult numbers
        data = {}     

#        filename = self.subDomain+'.csv'
#        if os.path.isfile(filename):
#            os.remove(filename)
#        self.file = open(filename,'w')

        self.req(self.main_url, data=data, method="GET", callback=self.process_html)

    def ignoreCallback(self, response):
        pass

    def process_html(self, response):

        if response.status_code == 200:

            data = {
                "x"                      : 0,
                "y"                      : 0,
                "pass_word"              : AirBase.sites[self.main_url]['pass'],
                "user_name"              : AirBase.sites[self.main_url]['user']
            }
            
            self.req(self.refer, data=data, method="POST", callback=self.after_login)
        else:
            # TODO: process bad response
            pass

    def after_login(self, response):
        if response.status_code == 200:
            data = {}
            def get_sidebar (response):
                self.req(self.intro_url, data=data, method="GET", callback=self.ignoreCallback)
                self.req(self.sidebar_url, data=data, method="GET", callback=self.do_quick_search)
            self.req(self.homePage_url, data=data, method="GET", callback=get_sidebar)
    
    def do_quick_search(self, response):
        if response.status_code == 200:
            data = {
                'items_category':    0,
                'items_itemName': '',    
                'x':    0,
                'y':    0
            }
            self.req(self.search_url, data=data, method="POST", callback=self.extract_info)
    
    def extract_info(self, response):     
        if response.status_code == 200:
            doc = ext_libs.BeautifulSoup(response.content)
            regex_products = re.compile(r"'vm\?task=([^']+function=displayItemDetail[^']+)'",re.MULTILINE)
            product = doc.find("a",attrs={"href":regex_products})
            #print categories
            if product:
                #self.file.write('|'.join(str(x) for x in AirBase.attributes_base.keys())+'\n')
                popupLink = regex_products.search(product['href'])
                if popupLink:
                    self.req(self.product_url+popupLink.groups()[0], data={}, method="GET", callback=self.process_products_link)

    def process_products_link(self, response):
        if response.status_code == 200:
            doc = ext_libs.BeautifulSoup(response.content, convertEntities=ext_libs.BeautifulSoup.HTML_ENTITIES)
            signInLink = doc.find('a',attrs={"target":'_parent', 'href':'welcome'})
            
            regex_nextPage = re.compile(r"=(.+function=nextDetailItem.+)",re.MULTILINE)
            nextPageButton = doc.find('a',attrs={"href":regex_nextPage})
            if nextPageButton:
                nextPageLink = regex_nextPage.search(nextPageButton['href'])
                self.req(self.product_url+nextPageLink.groups()[0], data={}, method="GET", callback=self.process_products_link)
            
            if signInLink:
                self.get_data(subDomain = self.subDomain)
            else:
                itemDetails = doc.find('tr',attrs={"bgcolor":'white'})
                if itemDetails:
                    recordTuple = list(AirBase.recordTupleBase)
                    details = itemDetails.findAll('td',text=True)
                    regex_code = re.compile(r'((^[0-9A-Z]+[\-\s]?)+[0-9A-Z]+)')
                    regex_code_label = re.compile(r'Item.+Code')
                    regex_price = re.compile(r'^\$([0-9]+(\.[0-9]+)?)')
                    regex_stock = re.compile(r'^(Yes|No)')
                    recordTuple[AirBase.attributes_base['DESCRIPTION']] = str(details[1]) if len(str(details[1])) > 3 else str(details[2])
                    recordTuple[AirBase.attributes_base['DESCRIPTION']] = recordTuple[AirBase.attributes_base['DESCRIPTION']].replace('\r\n', "").strip()
                    for index, detail in enumerate(details):
                        if regex_code.search(detail) and len(detail) > 0 and regex_code_label.search(details[index-1]):
                            recordTuple[AirBase.attributes_base['PARTNR.']] =  detail
                        elif regex_price.search(detail) and len(detail) > 0:
                            recordTuple[AirBase.attributes_base['RRP']] =  regex_price.search(detail).groups()[0]
                        elif regex_stock.search(detail) and len(detail) > 0 and len(details[index-1]) < 2:
                            recordTuple[AirBase.attributes_base['HLDS.']] =  detail
                    self.process_tuple(recordTuple)
                
    def format_date(self, utcTime):

        pass


if __name__ == "__main__":
    """ use for do quick test and check your code without involve the internal app execution routines """

    import logging
    logging.basicConfig(level=logging.NOTSET)

    # tell to app that avoid uses the internal execution routines
    VmNet.developer_mode = True
    # do query
    subDomains = ["crownkiwi","offroadimports","sevenee"]
    
    print "Script started."
    for subDomain in subDomains:
        VmNet.get_data(subDomain = subDomain)

    # wait till all the works are done
    VmNet.global_pool.join()
    print "Script finished."

    VmNet.send_files(subDomains)
