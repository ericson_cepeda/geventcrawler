__author__ = 'Ericson Cepeda <ericson.cepeda@gmail.com>'
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright (c) 2012, Viryan S.A.'

class Flight(dict):


    def add_class(self,name,price=None):
        if price!=None:
            assert type(price)==float

        classes = self.get("classes",{})
        classes[name] = {}
        if price!=None:
            classes[name]["price"] = price

        self.__setattr__("classes",classes)



    def __setattr__(self, key, value):
        self.__setitem__( key, value)

    def __getattr__(self, item):
        return self.__getitem__(item)