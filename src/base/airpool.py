__author__ = 'Ericson Cepeda <ericson.cepeda@gmail.com>'
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright (c) 2012, Viryan S.A.'

import ext_libs

class AirPool(object):
    """
        this class is a advanced pool for manage the concurrent requests.

        have a queue per airline that limits the maximum number of requests at the same time
        that  the app can do to one website

        also limit globally the total number of request at the same time that can be done
    """

    queues_container               = {}  #allow get access to all the queues
    active_requests_container      = {}  #allow get access to all the active requests counters

    global_pool_size       = 15 #maximum number of request at the same time than can be done globally
    global_pool            = ext_libs.Pool(global_pool_size)

    max_active_request     = 5  #max concurrent request by site, this properties can be override in each airline module
    logFile = None
    def __init__(self):
        self.logFile = open("exception.log",'w')
        #create the request queue if not exist
        # store queue in global container
        self.queues_container[self.name] =  self.queues_container.get(self.name,ext_libs.Queue())

        #allow to count the  current number of request that are being done to the airline site
        self.active_requests_container[self.name] = self.active_requests_container.get(self.name,0)

    def request_wrapper(self,request_call,*args,**kwargs):
        """ wrap a new request

            arguments:
            ==========
             - request_call  - function that will be wrapped
             - args, kwargs  - parameters of the function

            return
            ======
            wrapped function that will be called inner a greenlet
        """

        def worker():

            self.active_requests_container[self.name] += 1

            try:
                #excute request
                request_call(*args,**kwargs)
            except Exception as e:
                self.logFile.writelines(e.message)
                self.log.error("Something very bad is happening here :( !!!! :")
                self.log.error(e.message)
                self.log.error(ext_libs.traceback.format_exc())
            finally:
                self.active_requests_container[self.name] -= 1

            #if exist a item in the queue,  call it
            if not self.queues_container[self.name].empty():
                item = self.queues_container[self.name].get_nowait()
                self.spawn_request(item[0],*item[1],**item[2])
            else:
                #check if there exist a item in the global queue that needs to be executed
                for airline in self.queues_container:
                    request_queue = self.queues_container[airline]
                    if not request_queue.empty():
                        item  =request_queue.get_nowait()
                        self.spawn_request(item[0],*item[1],**item[2])
                        break

        #return wrapped request
        return worker


    def spawn_request(self,request_call,*args,**kwargs):
        """ create a new greenlet with a request if the number of active request allow that
            arguments:
            ==========
             - request_call - request that will be spawned
             - args, kwargs  - parameters of functions
        """

        #enqueu request if not exist space (if the pool if full or the maximum number of active request was archived)
        if (self.active_requests_container[self.name] >= self.max_active_request) or self.global_pool.full():
            self.queues_container[self.name].put([request_call,args,kwargs])
        else:
            #excecute work
            worker =  self.request_wrapper(request_call,*args,**kwargs)
            self.global_pool.spawn(worker)
