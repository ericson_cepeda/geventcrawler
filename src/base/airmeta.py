__author__    = 'Ericson Cepeda <ericson.cepeda@gmail.com>'
__credits__   = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__     = "dev@viryan.com"
__copyright__ = 'Copyright (c) 2012, Viryan S.A.'

class AirMeta(type):
    """
       For more info search about "metaclasses in python"
       class assembler for airlines
       allow add
            assertions
            more logging
            tracking
            dict translation for airports
            others stuff
       this class permits create easily modules for each airlines without worry about how the app is working internally !!!
       also fit the design of airline crawler modules
    """

    def __new__(cls, future_class_name, future_class_parents, future_class_attr):

        #Airpool properties that can not be overrides
        assert "queues_container" not in future_class_attr,"queues_container property can not be override"
        assert "active_requests_container" not in future_class_attr,"active_requests_container property can not be override"
        assert "global_pool_size" not in future_class_attr,"global_pool_size property can not be override"
        assert "global_pool" not in future_class_attr,"global_pool property can not be override"


        # if is the base class not modify anything
        if future_class_name=="AirBase":
            return super(AirMeta,cls).__new__(cls,future_class_name, future_class_parents, future_class_attr)


        #check the attrs that must be defined

        assert "name" in future_class_attr,"You need to define the name of the airline (name)"

        assert "domain" in future_class_attr,"You need to define the Internet domain of the airline (domain)"

        assert "search_url"  in future_class_attr,"You need to define the search URL of the airline"

        assert "get_data" in future_class_attr,"You need define the get_routes method (get_routes)"

        assert "format_date" in future_class_attr,"You need to define the format_date method (format_date)"




        #encapsule get_data
        get_data = future_class_attr.pop("get_data",None)
        future_class_attr["_get_data"] = get_data

        return super(AirMeta,cls).__new__(cls,future_class_name, future_class_parents, future_class_attr)
