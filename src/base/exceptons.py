__author__ = 'cristian'

class AirbaseException(Exception):
    """Base exception for airlines crawlers"""
    pass


class AirIOException(AirbaseException):
    """Base exception for airlines crawlers IO errors"""
    pass


class AirDBException(AirIOException):
    """Base exception for airlines crawlers relates with the db"""
    pass

class MissingAirport(AirDBException):
    """is throw when an airport is not covered or defined for a airline"""
    def __init__(self,airport,airline):
        self.message = "The airport {airport} is not covered or defined for {airline}".format(airport=airport,airline=airline)
    pass
