__authors__   = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__   = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__     = "dev@viryan.com"
__copyright__ = 'Copyright (c) 2012, Viryan S.A.'

import ftplib
import os
import re
from datetime import date
from base.airmeta import AirMeta
from base.exceptons import MissingAirport
from base.airpool import AirPool

import ext_libs
city_dictionary =  {}


class AirBase(AirPool):

    __metaclass__           = AirMeta #class assembler
    timeout                 = 10      #default timeout per connection (in secs)
    developer_mode          = False
    fileName                = "compilationFile"
    generalFile = None
    
    attributes_base = {
        'PARTNR.':0, 'DESCRIPTION':1, 'HLDS.':2, 'PL201':5, 'W/S':3, 'RRP':4, 'SPEC':5
    }
    
    generalHeaders = ['"sku"', '"description"', '"qty"', '"cost"', '"price"', '"special_price"', '"special_date"', '"status"']
    recordTupleBase = [''] * 8
    
    sites = {
        "http://www.dold.co.nz/"                                :    { "user": "motoone", "pass": "yz6yb"},
        "http://454.co.nz/dealer/"                              :    { "user": "motoone", "pass": "motooneD068"},
        "http://vm.net.nz/crownkiwi/welcome"                    :    { "user": "motoone", "pass": "motooneD068"},
        "http://vm.net.nz/offroadimports/welcome"               :    { "user": "motoone", "pass": "motooneD068"},
        "http://vm.net.nz/sevenee/welcome"                      :    { "user": "motoone", "pass": "motooneD068"},
        "http://darbi.sprint3.com/"                             :    { "user": "motoone", "pass": "dealer"},
        "http://eurobike.sprint3.com/"                          :    { "user": "motoone", "pass": "dealer"},
        "http://nationwide.sprint3.com/"                        :    { "user": "motoone", "pass": "yamaha100"},
        "http://northern.sprint3.com/"                          :    { "user": "moto1", "pass": "639335"},
        "http://whites.sprint3.com/"                            :    { "user": "motone", "pass": "180912man"},
        "http://eurotred.sprint3.com/"                          :    { "user": "motone", "pass": "240912nea"}
    }   

    def __init__(self):

        super(AirBase,self).__init__()
        #create logger for each airline
        self.log = ext_libs.logging.getLogger("aircrawler.app.airlines.{module_name}".format(module_name=self.name))
        #get the dictionary for the airports
        self.airport_dictionary = city_dictionary.get(self.name,{})
        # logging when the module is loaded
        self.log.info("module has been initialized")
        #====================create requests session - for manege cookies=============
        #create defaults headers
        headers = {}
        headers["Host"]            = self.host
        headers["Origin"]          = self.origin
        headers["Referer"]         = self.refer
        headers["User-Agent"]      = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"

        #config =  {"danger_mode": True,"pool_connections":100,"pool_maxsize":100,"max_redirects":0}

#        filename = self.fileName+'.csv'
#        if os.path.isfile(filename):
#            os.remove(filename)
#        self.file = open(filename,'w')
#        self.file.write(','.join(str(x) for x in AirBase.attributes_base.keys())+'\n')

        self.session = ext_libs.requests.session()


    def get_data_wrapper(self,**kargs):
        """
        arguments:
        ---------------------------------------------------------------
        - airport1(str) -> starting point, international name of the airport
        - airport2(str) -> ending  point, international name of the airport
        - day    (long) -> day in UTC tixxxxme

        return:
        ---------------------------------------------------------------
        - list of routes for that day, each class is a different route
        """
#        today = 0
#        assert airport1!=airport2
#        assert type(airport1) == str
#        assert type(airport2) == str
#        assert day>today
        self.log.info("searching ")
        #change international airport name to the needed for the page, the airport_dictionary is defined in AirBase
        try:
            print "wrapper"
#            a1 = self.airport_dictionary[airport1]
#            a2 = self.airport_dictionary[airport2]
        except KeyError as e:
            error = MissingAirport(e.message,self.name)
            self.log.error(error.message)
            raise error

        # change day format  to the needed for the page
#        d = self.format_date(day)
        routes = self._get_data()
        self.log.info()
        return routes

    @classmethod
    def get_data(cls,**kwargs):
        """
          this functions intercept  the call of get_data in each airline module and create a object instance that have
          its own session (for cookies)

          this is important for fix cookies issues when the app do  queries to a site at the same time with different parameters
        """
        new_instance = cls()

        if new_instance.developer_mode:
            #if developer mode is active call get_data directly
            worker = new_instance._get_data
        else:
            #all wrapped get_data
            worker = new_instance.get_data_wrapper()

        #execute worker
        new_instance.generalFile = kwargs.get("compilationFile", None)
        worker(**kwargs)


    def format_date(self,**kwargs):
        """
          # change day format  to the needed for the page
        """
        raise NotImplementedError


    def req(self,*args,**kwargs):

        #get method
        new_args = [kwargs.pop("method","GET")]
        new_args.extend(args)
        args = new_args
        data = kwargs.get("data",{})
        self.log.info("doing {method} request to {url} with data = {data}".format(url=args[1],method=args[0],data=ext_libs.json.dumps(data,ensure_ascii=False)),extra={"event":"Request","url":args[1],"data":data,"method":args[0]})
        #callback
        callback = kwargs.pop("callback")
        #spawn request
        def wrap():
            response = self.session.request(*args,**kwargs)
            callback(response)
        self.spawn_request(wrap)

    @staticmethod
    def send_files(subDomains):
        print "Sending files..."
        remoteFolder = '/var/www/moto1.co.nz/htdocs/magmi/import/'
        s = ftplib.FTP('113.21.225.138','darkroom1','darkroom1') # Connect 113.21.225.138
        for subDomain in subDomains:
            filename = subDomain+'.csv'
            try:
                if os.path.isfile(filename):
                    f = open(subDomain+'.csv','rb')          # file to send
                    s.storbinary('STOR '+remoteFolder+subDomain+'.csv', f)         # Send the file
                    f.close()                                # Close file and FTP
            except Exception as e:
                print e.message
                continue
        s.quit()
        print "Files sent."
        
    def process_tuple(self,recordTuple):
        regex_available = re.compile(r'(>[0-9]+|^Available|^Yes)')
        regex_not_available = re.compile(r'(^No[t]?)')
        regex_low_available = re.compile(r'(^Low)')
        
        matchAvailable = regex_available.search(recordTuple[AirBase.attributes_base['HLDS.']])
        matchNotAvailable = regex_not_available.search(recordTuple[AirBase.attributes_base['HLDS.']])
        matchLow = regex_low_available.search(recordTuple[AirBase.attributes_base['HLDS.']])
        recordTuple[7] = 'enabled'
        if matchAvailable:
            recordTuple[AirBase.attributes_base['HLDS.']] = '10'
        elif matchNotAvailable:
            recordTuple[AirBase.attributes_base['HLDS.']] = '0'
            recordTuple[7] = 'disabled'
        elif matchLow:
            recordTuple[AirBase.attributes_base['HLDS.']] = '2'
        priceAttributes = [AirBase.attributes_base['SPEC'],AirBase.attributes_base['W/S'],AirBase.attributes_base['RRP']]
        for attributeIndex in priceAttributes:
            #if isinstance(recordTuple[attributeIndex], str):
            string_process = str(recordTuple[attributeIndex])
            string_process = re.sub(r",", "", string_process)
            string_process = re.sub(r"\$", "", string_process)
            recordTuple[attributeIndex] = string_process
            if attributeIndex == AirBase.attributes_base['RRP'] and re.compile(r'(^0)').search(string_process):
                recordTuple[7] = 'disabled'
        regex_spec_exists = re.compile(r'[0-9]+(\.[0-9]+)?')
        recordExpectedSpecial = str(recordTuple[AirBase.attributes_base['SPEC']])
        if regex_spec_exists.search(recordExpectedSpecial):
            rrp = AirBase.process_price(recordTuple[AirBase.attributes_base['RRP']])
            w_s = AirBase.process_price(recordTuple[AirBase.attributes_base['W/S']])
            spec = float(recordTuple[AirBase.attributes_base['SPEC']])
            specialPrice = rrp - w_s + spec
            if specialPrice < 0:
                print self.origin
                print recordTuple[0]
                print str(rrp)+"-"+str(w_s)+"+"+str(spec)
            recordTuple[AirBase.attributes_base['SPEC']] = specialPrice
            todayInfo = date.today()
            recordTuple[6] = str(todayInfo.day) +'/'+ str(todayInfo.month) +'/'+ str(todayInfo.year)
        else:
            recordTuple[6] = ""
        recordTuple = "|".join("\""+re.sub(r'"', "", str(x))+"\"" for x in recordTuple)
        recordTuple = re.sub(r",", ";", recordTuple)
        recordTuple = re.sub(r"\|", ",", recordTuple)
        try:
            self.generalFile.write(recordTuple.decode('iso-8859-1').encode('utf8')+'\n')# python will convert \n to os.linesep
        except Exception as e:
            pass
    
    @staticmethod    
    def process_price(price):
        if len(str(price)) == 0:
            return 0
        return float(price)
