__authors__   = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__   = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__     = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

from gevent.queue import Queue
from gevent.pool import  Pool
import  traceback
import gevent
from gevent import monkey
monkey.patch_all(select=False,thread=False)
from gevent.pool import Pool
from BeautifulSoup import BeautifulSoup
import json
import logging
import requests
