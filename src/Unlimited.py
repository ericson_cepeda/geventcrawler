# -*- coding: iso-8859-15 -*-
__authors__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

#import os
import re
from base.airbase import AirBase
import ext_libs
from base.exceptons import AirIOException

class EasyFlyApiBadResponse(AirIOException):
    """
        when this page (http://www.easyfly.com.co/desktop/wsDiscriminacion) have bad response, raise a error

    """
    pass

class Unlimited(AirBase):

    name = "Unlimited"
    domain = "454.co.nz"
    host = "454.co.nz"
    origin = "http://454.co.nz"
    refer = "http://454.co.nz/dealer/login/"
    main_url = "http://454.co.nz/dealer/"
    search_url = 'http://454.co.nz/dealer/search/'
    csrfmiddlewaretoken = ''
    subDomain = 'unlimited'

    def get_data(self, **kwargs):

        """
            see AirBase.get_data_wrapper definition
            return
                {dict} the flights of Unlimited for a particular day, route an adult numbers
s        """

        # fill form for query the available flights for a day, route and adult numbers
        data = {}     

#        filename = self.subDomain+'.csv'
#        if os.path.isfile(filename):
#            os.remove(filename)
#        self.file = open(filename,'w')

        self.req(self.main_url, data=data, method="GET", callback=self.process_html)

    def process_html(self, response):

        if response.status_code == 200:

            doc = ext_libs.BeautifulSoup(response.content)
            
            csrfmiddlewaretoken = doc.find("input",attrs={"name":'csrfmiddlewaretoken'})
            
            self.csrfmiddlewaretoken = csrfmiddlewaretoken['value']
            
            data = {
                "csrfmiddlewaretoken"   : csrfmiddlewaretoken['value'],
                "next"                  : "",
                "password"              : AirBase.sites[self.main_url]['pass'],
                "username"              : AirBase.sites[self.main_url]['user']
            }
            
            self.req(self.refer, data=data, method="POST", callback=self.after_login)
        else:
            # TODO: process bad response
            pass

    def after_login(self, response):
        if response.status_code == 200:
            data = {
                'category'              : '',
                'csrfmiddlewaretoken'   : self.csrfmiddlewaretoken,
                'model'                 : '',
                'search'                : '',
                'searchtype'            : 'Search',
                'year'                  : ''   
            }
            self.req(self.search_url,data=data,callback=self.process_search)

    def process_search(self,response):
        doc = ext_libs.BeautifulSoup(response.content, convertEntities=ext_libs.BeautifulSoup.HTML_ENTITIES)
        regex_nextPage = re.compile(r"(next)$",re.MULTILINE)
        #regex_nextPage_href = re.compile(r"(page/[0-9]+)$",re.MULTILINE)
        nextPageButtons = doc.findAll("a")
        for nextPageButton in nextPageButtons:
            buttonText = nextPageButton.getText()
            matchNextPageLink = regex_nextPage.search(buttonText)
            if matchNextPageLink:
                self.req(self.origin+nextPageButton['href'], data={}, method="GET", callback=self.process_search)
                break
        
        productsTable = doc.find('table',attrs={'id':'products'})
        if productsTable:
            
            regex_code = re.compile(r'Code:\s?([0-9A-Z/]+)$',re.MULTILINE)
            regex_price = re.compile(r'\$([0-9]+\.[0-9]+)',re.MULTILINE)
            regex_stock = re.compile(r'(yes|no)',re.MULTILINE)
            
            productsTableBody = productsTable.find('tbody')
            products = productsTableBody.findAll('tr')
            for product in products:
                recordTuple = list(AirBase.recordTupleBase)
                description = product.find('h2')
                code = product.find('span',attrs={'class':'product-code'})
                price = product.find('dl',attrs={'class':'product-price'})
                stock = product.find('td',attrs={'class':'instock'})
                recordTuple[AirBase.attributes_base['DESCRIPTION']] = description.getText()
                
                match = regex_code.search(code.getText())
                recordTuple[AirBase.attributes_base['PARTNR.']]= match.groups()[0] if match else ':00000000'
                #recordTuple['PARTNR.'] += '|'
                
                match = regex_price.findall(price.getText())
                recordTuple[AirBase.attributes_base['RRP']]= str(match[0]) if match else '0.0'
                #recordTuple['RRP'] += '|'
                
                match = regex_stock.findall(str(stock))
                recordTuple[AirBase.attributes_base['HLDS.']]= '10' if match else '0'
                #recordTuple['HLDS.'] += '|'
                
                self.process_tuple(recordTuple)
        
    def format_date(self, utcTime):

        pass


if __name__ == "__main__":
    """ use for do quick test and check your code without involve the internal app execution routines """

    import logging
    logging.basicConfig(level=logging.NOTSET)

    # tell to app that avoid uses the internal execution routines
    Unlimited.developer_mode = True
    # do query
    print "Script started."
    Unlimited.get_data()

    # wait till all the works are done
    Unlimited.global_pool.join()
    print "Script finished."

    Unlimited.send_files([Unlimited.subDomain])