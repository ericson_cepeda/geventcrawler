# -*- coding: iso-8859-15 -*-
__authors__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__credits__ = ['Ericson Cepeda <ericson.cepeda@gmail.com>']
__email__ = "dev@viryan.com"
__copyright__ = 'Copyright 2012, Viryan S.A.'

#import os
import re
from base.airbase import AirBase
import ext_libs
from base.exceptons import AirIOException

class EasyFlyApiBadResponse(AirIOException):
    """
        when this page (http://www.easyfly.com.co/desktop/wsDiscriminacion) have bad response, raise a error

    """
    pass

class Dold(AirBase):

    name = "Dold"
    domain = "dold.co.nz"
    host = "www.dold.co.nz"
    origin = "http://www.dold.co.nz"
    refer = "http://www.dold.co.nz/"
    main_url = "http://www.dold.co.nz/"
    home_url = 'http://www.dold.co.nz/default.aspx'
    search_url = 'http://www.dold.co.nz/DealerSearch.aspx/?search='
    subDomain = 'Dold'

    event_target    = ""
    event_argument  = ""
    view_state      = ""
    event_validation= ""

    def get_data(self, **kwargs):

        """
            see AirBase.get_data_wrapper definition
            return
                {dict} the flights of Dold for a particular day, route an adult numbers
s        """

        # fill form for query the available flights for a day, route and adult numbers
        
        data = {}     

        self.req(self.main_url, data=data, method="GET", callback=self.process_html)

    def process_html(self, response):

        if response.status_code == 200:

            doc = ext_libs.BeautifulSoup(response.content)
            
            self.event_target           = '__EVENTTARGET'
            self.event_argument         = '__EVENTARGUMENT'
            self.event_validation       = '__EVENTVALIDATION'
            self.view_state             = '__VIEWSTATE'
            
            for hiddenInput in doc.findAll("input",attrs={'type':'hidden'}):
                if hiddenInput['name'] == self.event_target:
                    self.event_target = hiddenInput['value']
                elif hiddenInput['name'] == self.event_argument:
                    self.event_argument = hiddenInput['value']
                elif hiddenInput['name'] == self.event_validation:
                    self.event_validation = hiddenInput['value']
                elif hiddenInput['name'] == self.view_state:
                    self.view_state = hiddenInput['value']            
            
            data = {
                '__ASYNCPOST'       : True,
                '__EVENTARGUMENT'   : self.event_argument,
                '__EVENTTARGET'     : self.event_target,
                '__EVENTVALIDATION' : self.event_validation,	
                '__VIEWSTATE'       : self.view_state,
                'ctl00$LoginBtn.x'  : '0',
                'ctl00$LoginBtn.y'  : '0',
                'ctl00$Password'    : AirBase.sites[self.main_url]['pass'],
                'ctl00$SearchBox'   : '',
                'ctl00$Username'    : AirBase.sites[self.main_url]['user'],
                'ctl00$ctl08'       : 'ctl00$CartUpdatePanel|ctl00$LoginBtn'
            }
            
            self.req(self.refer, data=data, method="POST", callback=self.after_login)
        else:
            # TODO: process bad response
            pass

    def after_login(self, response):
        if response.status_code == 200:
            #self.file.write('|'.join(str(x) for x in AirBase.attributes_base.keys())+'\n')
            
            def ignore_home(response):
                self.req(self.search_url,data={},callback=self.process_search)
            self.req(self.home_url,data={},callback=ignore_home)
            
    def process_search(self,response):
        doc = ext_libs.BeautifulSoup(response.content, convertEntities=ext_libs.BeautifulSoup.HTML_ENTITIES)
        productsTable = None
        for table in doc.findAll("table"):
            if 'class' in dict(table.attrs) and table['class'] == 'table':
                productsTable = table
                break
        if productsTable:
            pagerContainer = doc.find('div',attrs={'class':'pager'})
            if pagerContainer:
                regex_pager = re.compile(r'ResultsDataPager')
                pagesLinkContainer = pagerContainer.find('span',attrs={'id':regex_pager})
                if pagesLinkContainer:
                    currentPage = int(pagesLinkContainer.find('span').getText())
                    nextPage = currentPage+1
                    regex_navigation = re.compile("page=("+str(nextPage)+"){1}$")
                    navigationLink = pagesLinkContainer.find("a",attrs={"href":regex_navigation})
                    if navigationLink:
                        self.req(self.origin+navigationLink['href'], data={}, method="GET", callback=self.process_search)
            
            products = productsTable.findAll('tr',recursive=False)
            for product in products:
                recordTuple = list(AirBase.recordTupleBase)
                productAttributes = product.findAll('td',text=True)
                try:
                    regex_price = re.compile(r'[A-Z]+:\s\$(.*)$')
                    recordTuple[AirBase.attributes_base['DESCRIPTION']] = productAttributes[6]
                    recordTuple[AirBase.attributes_base['PARTNR.']]     = productAttributes[2]
                    recordTuple[AirBase.attributes_base['W/S']]         = regex_price.search(productAttributes[14]).groups()[0]
                    recordTuple[AirBase.attributes_base['RRP']]         = regex_price.search(productAttributes[15]).groups()[0]
                    recordTuple[AirBase.attributes_base['HLDS.']]       = productAttributes[12]
                    #self.file.write('|'.join(str(x) for x in recordTuple.values())+'\n')
                    self.process_tuple(recordTuple)
                except Exception as e:
                    #self.log.error("Dold expected exception: "+e.message)
                    continue
        
    def format_date(self, utcTime):

        pass


if __name__ == "__main__":
    """ use for do quick test and check your code without involve the internal app execution routines """

    import logging
    logging.basicConfig(level=logging.NOTSET)

    # tell to app that avoid uses the internal execution routines
    Dold.developer_mode = True
    # do query
    print "Script started."
    Dold.get_data()

    # wait till all the works are done
    Dold.global_pool.join()
    print "Script finished."

    Dold.send_files([Dold.subDomain])